include Nanoc::Helpers::Blogging
include Nanoc::Helpers::Tagging
include Nanoc::Helpers::Rendering
include Nanoc::Helpers::LinkTo
require 'json'
module Helpers
    def get_pretty_date(post)
        attribute_to_time(post[:created_at]).strftime('%-d %B, %Y')
    end

    def get_post_start(post)
        content = post.compiled_content

        if content.size > 100
            content = content.split(" ").each_with_object("") {|x,ob| break ob unless (ob.length + " ".length + x.length <= 100);ob << (" " + x)}.strip
        end
        
        content = content.gsub(/((<img src="(.*)"><\/img>)|(<img src="(.*)">))/i, '(image)')
        
        return content
    end

    def get_services()
        services_file = File.read('content/assets/data/services.json')
        services = JSON.parse(services_file)

        return services
    end
end

include Helpers