<h2>Build Status</h2>
Build: [![build status](https://gitlab.com/roconnor/roconnor.gitlab.io/badges/master/build.svg)](https://gitlab.com/roconnor/roconnor.gitlab.io/commits/master)
<h2>What is it?</h2>
This repo is the root site for my network. Found [here](http://roconnor.gitlab.io/landing), this site has details from all my public projects and blogs.
<h2>Who is it for</h2>
If you're on here, it's for you! This network is directed at anyone who enjoys technology, no matter what sector.